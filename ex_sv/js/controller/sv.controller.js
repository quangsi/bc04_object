function layThongTinTuForm() {
  const maSv = document.getElementById("txtMaSV").value;
  const tenSv = document.getElementById("txtTenSV").value;
  const loaiSv = document.getElementById("loaiSV").value;
  const diemToan = document.getElementById("txtDiemToan").value * 1;
  const diemVan = document.getElementById("txtDiemVan").value * 1;
  var sv = {
    id: maSv,
    name: tenSv,
    type: loaiSv,
    math: diemToan,
    literature: diemVan,
    getScore: function () {
      return (this.math + this.literature) / 2;
    },
    getRank: function () {
      var dtb = this.getScore();
      if (dtb > 5) {
        return "Good";
      } else {
        return "Bad";
      }
    },
  };
  return sv;
}

function showThongTinLenForm(sv) {
  document.getElementById("spanTenSV").innerText = sv.name;
  document.getElementById("spanMaSV").innerText = sv.id;
  document.getElementById("spanLoaiSV").innerText = sv.type;
  document.getElementById("spanDTB").innerText = sv.getScore();
  document.getElementById("spanXepLoai").innerText = sv.getRank();
}
