function SinhVien(maSv, tenSv, loaiSV, diemToan, diemVan) {
  this.id = maSv;
  this.name = tenSv;
  this.type = loaiSV;
  this.math = diemToan;
  this.literature = diemVan;
  this.getScore = function () {
    return (this.math + this.literature) / 2;
  };
  this.getRank = function () {
    var dtb = this.getScore();
    if (dtb > 5) {
      return "Good";
    } else {
      return "Bad";
    }
  };
}

function Cat(name, age) {
  this.name = name;
  this.age = age;
}

var milo = new Cat("Milo", 3);
milo.name;
milo.age;

var black = new Cat("White", 5);
black.name;
