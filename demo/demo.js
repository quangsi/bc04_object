// objecdt ~ array : pass by reference

// function sayHello(friend) {
//   console.log("Meo meo " + friend);
// }
const pull = {
  name: "pull",
  // property
  age: 2,
  gender: "Male",
  address: "Cao thang",
  bark: function (friend) {
    console.log("Meo meo " + friend);
  },
  // method
  children: ["đen", "vàng", "đỏ"],
  wife: {
    name: "bom",
    age: 1.5,
    gender: "Femail",
  },
  isMarried: true,
};
console.log(pull.wife);

pull.name = "Milo";

// dynamic key
var key = "gender";
pull[key];

console.log(pull["name"]);

pull.bark("Alice");

const a = 2;

var b = a;
b = 5;
var c = b;

var miu = pull;
console.log("pull.name", pull.name);
miu.name = "Miuuuu";

miu.age = 20;

var bull = miu;
console.log("miu.name : ", miu.name);
console.log("pull.name", pull.name);
console.log("pull.age", pull.age);

const arrNum = [2, 4, 6];
arrNum.push(10);

// arrNum=[]
